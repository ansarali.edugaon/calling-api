package com.example.callingapi.models

import com.google.gson.annotations.SerializedName

data class LogInModels (
    @SerializedName("current_user")
    val currentUser :CurrentUser,

    @SerializedName("csrf_token")
    val csrfToken :String? = null,

    @SerializedName("logout_token")
    val logoutToken :String? = null
)

data class CurrentUser (
    @SerializedName("uid")
    val uid :String? = null,

    @SerializedName("roles")
    val roles : List<String>
)

data class LogInRequest (
    @SerializedName("name")
    val name :String,

    @SerializedName("pass")
    val pass :String
)

