package com.example.callingapi.api

import com.example.callingapi.models.LogInModels
import com.example.callingapi.models.LogInRequest
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface LogInApi {

    @Headers("Content-Type: application/json")
    @POST("user/login?_format=json")
    fun getLogInToken(@Body logInRequest: LogInRequest): Observable<LogInModels>
    companion object Factory{
        fun create():LogInApi{
            val BASE_URL = "http://edugaonlabs.com"
            val retrofit= Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()

            return (retrofit.create(LogInApi::class.java))
        }
    }
}