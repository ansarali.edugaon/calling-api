package com.example.callingapi.api

import com.example.callingapi.models.Models
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface Api {
    @GET("rest/session/token")
    fun getToken(): Observable<Models>
    companion object Factory{
        fun create():Api{
            val BASE_URL = "http://edugaonlabs.com/"
            val retrofit= Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return (retrofit.create(Api::class.java))
        }
    }
}