package com.example.callingapi.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.callingapi.R
import com.example.callingapi.api.LogInApi
import com.example.callingapi.models.LogInRequest
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_log_in.*

class LogInActivity : AppCompatActivity() {
    lateinit var nameEditText: EditText
    lateinit var passwordEditText: EditText
    lateinit var logInButton: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)

        nameEditText = findViewById(R.id.name_EditText)
        passwordEditText = findViewById(R.id.password_EditText)
        logInButton = findViewById(R.id.logIn_Button)

        val register = findViewById<TextView>(R.id.register_TextView)
        register.setOnClickListener {
            val intent = Intent(this,RegisterActivity::class.java)
            startActivity(intent)
        }

        logInButton.setOnClickListener {

            loadLogInToken()
        }
    }

    private fun loadLogInToken() {
        val name = nameEditText.text.toString()
        val password = passwordEditText.text.toString()

        if (name.isEmpty()){
            name_EditText.error = "Please Enter Name!"
            name_EditText.requestFocus()
            return
        }

        if (password.isEmpty()){
            password_EditText.error = "Please Enter Password!"
            password_EditText.requestFocus()
            return
        }


        val loginRequest = LogInRequest(name,password)

        val ServiceLogIn = LogInApi.create()
        ServiceLogIn.getLogInToken(loginRequest)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({result ->
               result.csrfToken?.let { saveLogInToken(it) }
               val intent= (Intent(this,HomeActivity::class.java))
                startActivity(intent)
                finish()
                result.logoutToken?.let { saveLogOutToken(it) }

            }, {error ->
                Toast.makeText(this,"Something went Wrong!",Toast.LENGTH_SHORT).show()
                error.printStackTrace()
            }
            )


    }

    private fun saveLogInToken(logInToken:String){
        val preference =this.getPreferences(Context.MODE_PRIVATE)?:return
        with(preference.edit()){
            putString("logIn_token",logInToken)
            commit()
        }
    }

    private fun saveLogOutToken(logOutToken:String){
        val preference = getPreferences(Context.MODE_PRIVATE)?:return
        with(preference.edit()){
            putString("logOut_token",logOutToken)
            commit()
        }
    }
}