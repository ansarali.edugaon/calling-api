package com.example.callingapi.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Models(
    @Expose
    @SerializedName("token")
    val entryToken:String? =null
)