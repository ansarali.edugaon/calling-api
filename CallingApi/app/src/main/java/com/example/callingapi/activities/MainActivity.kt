package com.example.callingapi.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.callingapi.R
import com.example.callingapi.api.Api
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        checkToken()
    }

    private fun checkToken() {
        val sharedPreferences = this.getPreferences(Context.MODE_PRIVATE)
        val sessionToken = sharedPreferences.getString("session_token",null)

        if (sessionToken!=null){
            val intent = Intent(this,LogInActivity::class.java)
            startActivity(intent)
            finish()
        }else{
            loadToken()
        }
    }


    private fun loadToken(){
        val ServiceApi = Api.create()
        ServiceApi.getToken()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({result ->
                saveToken(result.entryToken)

            }, {error ->
                error.printStackTrace()
            }
            )
    }

    private fun saveToken(token: String?) {
        val sharedPreferences = this.getPreferences(Context.MODE_PRIVATE)?:return
        with(sharedPreferences.edit()){
            putString("session_token",token)
            commit()
        }
    }
}